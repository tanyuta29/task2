package com.company.details;

public class Engine {
    private int power;
    private String producer;

    public Engine() {
        this.producer = producer;
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String toString() {
        return "Engine: " + "Power: " + getPower() + "Producer" + getProducer();
    }
}

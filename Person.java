package com.company.entities;

public class Person {
    private int age;
    private String nameSurname;
    private String gender;
    private int phoneNumber;

    public Person(String gender, String nameSurname, int age, int phoneNumber) {
        this.nameSurname = nameSurname;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.age = age;
    }

    public Person() {

    }

    public String getNameSurname() {
        return nameSurname;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setAge() {
        this.age = age;
    }

    public void setNameSurname() {
        this.nameSurname = nameSurname;
    }

    public void setGender() {
        this.gender = gender;
    }

    public void setPhoneNumber() {
        this.phoneNumber = phoneNumber;
    }

    public String toString() {
        return "Person information: " + "Name Surname: " + getNameSurname() + "" +
                "Gender: " + getGender() +
                "Phone number: " + getPhoneNumber() +
                "Age: " + getAge();
    }
}

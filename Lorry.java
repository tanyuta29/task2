package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Lorry extends Car {
    public int loadCapacity;

    public Lorry(String carBrand, String carClass, int weight, int loadCapacity) {
        super(carBrand, carClass, weight);
        this.loadCapacity = loadCapacity;

    }

    public Driver getDriver() {
        return super.getDriver();
    }

    public Engine getEngine() {
        return super.getEngine();
    }

    public int getWeight() {
        return super.getWeight();
    }

    public String getCarBrand() {
        return super.getCarBrand();
    }

    public String getCarClass() {
        return super.getCarClass();
    }

    public int getLoadCapacity() {
        return loadCapacity;
    }

    public void setLoadCapacity(int loadCapacity) {
        this.loadCapacity = loadCapacity;
    }

    public String toString() {
        return super.toString()
                + "Load capacity: " + getLoadCapacity()
                + "car brand: " + carBrand
                + " Class: " + carClass
                + " Driver: " + driver
                + " Engine: " + engine ;
    }
}
